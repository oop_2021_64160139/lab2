public class DataTypes1 {
    public static void main(String[] args) {
        int name = 15 ;  // Integer (whole number)
        float floatname = 5.99f;  // Floating point number
        char myLetter = 'D'; // Character
        boolean myBool = true ;  // Boolean
        String text = "Hello";  // String
    }
}
