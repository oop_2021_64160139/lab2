public class TypeCasting2 {
    public static void main(String[] args) {
        double mydou = 9.78d;
        int myInt = (int) mydou;     // Manual casting: double to int
        System.out.println(mydou);   // Outputs 9.78
        System.out.println(myInt);      // Outputs 9
    }
}
