public class TypeCasting1 {
    public static void main(String[] args) {
        int n = 9 ;
        double mydou = n; // Automatic casting: int to double
        System.out.println(n); // Outputs 9
        System.out.println(mydou);  // Outputs 9.0
    } 
}
