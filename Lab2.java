import java.util.Scanner;
public class Lab2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("please enter a number = ");
        int numx = sc.nextInt();

        
        if( numx >= 80){
            System.out.println("A");
        }
        else if (numx < 80 && numx >= 75 ){
            System.out.println("B+");
        }
        else if (numx < 75 && numx >= 70 ){
            System.out.println("B");
        }
        else if (numx < 70 && numx >= 65 ){
            System.out.println("C+");
        }
        else if (numx < 65 && numx >= 60 ){
            System.out.println("C");
        }
        else if (numx < 60 && numx >= 55 ){
            System.out.println("D+");
        }
        else if (numx < 55 && numx >= 50 ){
            System.out.println("D");
        } 
        else {
            System.out.println("F");
        }
    }
}
